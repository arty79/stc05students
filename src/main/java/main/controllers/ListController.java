package main.controllers;

import main.model.pojo.Student;
import main.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Artem Panasyuk on 27.04.2017.
 */

@Controller
@RequestMapping(value = "/listStudents")
public class ListController {
    @Autowired
    public StudentService studentService;

    @RequestMapping(method = RequestMethod.GET)
    public String showList(Model model) {
        model.addAttribute("students", studentService.getAllStudents());
        return "listStudents";
    }

    @RequestMapping(value = "/students/students?edit={id}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable long id){
        Student student = studentService.getById(id);
        return new ModelAndView("addStudent","command", student);
    }

    /**
     * Setter for property 'studentService'.
     *
     * @param studentService Value to set for property 'studentService'.
     */
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }
}
