package main.controllers;

import main.model.pojo.Student;
import main.services.StudentService;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 *
 */
public class StudentsServlet extends HttpServlet {

    //@Autowired
    private StudentService studentService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Student> students = studentService.getAllStudents();

        req.setAttribute("students", students);

        getServletContext().getRequestDispatcher("/listStudents.jsp")
                .forward(req, resp);
    }

    /**
     * Setter for property 'studentService'.
     *
     * @param studentService Value to set for property 'studentService'.
     */
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }
}
