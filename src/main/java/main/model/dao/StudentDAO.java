package main.model.dao;

import main.model.pojo.Student;

import java.util.Collection;

/**
 *
 */
public interface StudentDAO extends DAO<Long, Student> {
    Collection<Student> getAll();
    Student getById(Long id);
    Long insert(Student entity);
    void delete(Student entity);

}
