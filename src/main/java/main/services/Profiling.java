package main.services;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Artem Panasyuk on 26.04.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Profiling {

}
