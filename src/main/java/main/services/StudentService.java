package main.services;

import main.model.pojo.Student;

import java.util.Collection;
import java.util.List;

/**
 *
 */
public interface StudentService {

   List<Student> getAllStudents();

   long saveStudent(Student student);

   Collection<Student> getAll();
   Student getById(Long id);
   Long insert(Student entity);
   void delete(Student entity);
}
