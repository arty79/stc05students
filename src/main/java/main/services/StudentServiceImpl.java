package main.services;

import main.model.dao.StudentDAO;
import main.model.pojo.Student;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 *
 */
@Profiling
@Service
@Scope("prototype")
public class StudentServiceImpl implements StudentService {


    static {
        PropertyConfigurator.configure(StudentServiceImpl.class.getClassLoader()
                .getResource("log4j.properties"));
    }

    @Autowired
    private StudentDAO studentDAO;

    @Override
    public List<Student> getAllStudents() {
        List<Student> students = new ArrayList<>();
        students.addAll(studentDAO.getAll());
        students.sort(Comparator.comparingLong(Student::getId));
        return students;
    }

    @Override
    public long saveStudent(Student student) {
        return studentDAO.insert(student);
    }

    @Override
    public Collection<Student> getAll() {
        return studentDAO.getAll();
    }

    @Override
    public Student getById(Long id) {
        return studentDAO.getById(id);
    }

    @Override
    public Long insert(Student entity) {
        return studentDAO.insert(entity);
    }

    @Override
    public void delete(Student entity) {
        studentDAO.insert(entity);
    }


    public StudentDAO getStudentDAO() {
        return studentDAO;
    }


    public void setStudentDAO(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }
}
