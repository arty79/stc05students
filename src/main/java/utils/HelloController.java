package utils;

import main.model.pojo.User;
import main.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Artem Panasyuk on 27.04.2017.
 */

@Controller
@SessionAttributes("user")
public class HelloController {
    @Autowired
    public UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String sayHello() {
        return "login";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView login(@RequestParam(value = "login", required = true) String login,
                              @RequestParam(value = "password", required = true) String password) {
        ModelAndView mav = new ModelAndView();
        if (userService.auth(login, password) != null) {
            User user = new User(login);
            mav.addObject("user", user);
            mav.setViewName("redirect:listStudents");
        } else {
            mav.setViewName("redirect:/");
        }
        return mav;
    }
}
