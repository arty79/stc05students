package utils;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Artem Panasyuk on 26.04.2017.
 */
@Component
public class LoggerBean {
   private List<String> list = new CopyOnWriteArrayList<>();

   public void writeLog(String e){
       list.add(e);
   }

}
