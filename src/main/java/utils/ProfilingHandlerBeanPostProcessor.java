package utils;

import main.services.Profiling;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Artem Panasyuk on 26.04.2017.
 */
public class ProfilingHandlerBeanPostProcessor implements BeanPostProcessor {
    Map<String, Class> map = new HashMap<>();
    private final Logger LOG = Logger.getLogger(ProfilingHandlerBeanPostProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Profiling.class)) {
            map.put(beanName, beanClass);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = map.get(beanName);
        if (beanClass != null) {
            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    LOG.debug("Profiling");
                    long before = System.nanoTime();
                    //Object retVal = method.invoke(bean, args);
                    long after = System.nanoTime();
                    String msg = after - before + "";
                    LOG.debug(msg);
                    LOG.debug("End Profiling");
                    return null;
                }
            });
        }
        return bean;
    }
}
