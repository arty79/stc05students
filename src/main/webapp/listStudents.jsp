<!--
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8"/>
    <title>List</title>
</head>
<link>
<h3>Students list</h3>
<c:if test="${pageContext.request.userPrincipal.name != null}">
    <h2>Welcome : ${pageContext.request.userPrincipal.name}
        | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a></h2>
</c:if>
<table>
<c:forEach items="${students}" var="student">
    <tr>
        <td><c:out value="${student.id}"></c:out></td>
        <td><c:out value="${student.name}"></c:out></td>
        <td><c:out value="${student.age}"></c:out></td>
        <td><c:out value="${student.groupId}"></c:out></td>

        <td><a href="${pageContext.request.contextPath}/students?edit=${student.id}">edit</a>
            <a href="${pageContext.request.contextPath}/students?delete=${student.id}" onclick="if (!confirm('Are you sure?')) return false;">delete</a></td>
    </tr>
</c:forEach>
</table>
<a href="addStudent.jsp">Add student</a>
</body>
</html>
